package com.hastos.Lesson1.Task1;

public class Human {

    String name;
    String lastName;
    String surName;

    public Human(String name, String lastName, String surName) {
        this.name = name;
        this.lastName = lastName;
        this.surName = surName;
    }

    public Human(String name, String lastName) {
        this.name = name;
        this.lastName = lastName;
    }

    private Human getFullName() {
        return new Human("Viktor ", "Shapoval ", "Mikhailovich");
    }

    private String getShortName() {
        String allInitials = lastName + name + surName;
        if (surName == null) {
            allInitials = lastName + name;
        }
        return allInitials.replaceAll("(\\p{L}+|\\G)(\\h+\\p{L})\\p{L}*", "$1$2.");
    }
}

