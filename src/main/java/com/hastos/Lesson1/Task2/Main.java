package com.hastos.Lesson1.Task2;

public class Main {
    public static void main(String[] args) {
        checkPointInCircle();
    }

    public static void checkPointInCircle() {
        PointList list = new PointList();
        list.addNewPoint();
        Circle circle = new Circle();
        for (Point point : list.pointList) {
            double number =
                    Math.sqrt(Math.pow((point.getX() - circle.getCentre().getX()), 2)
                            + Math.pow((point.getY() - circle.getCentre().getY()), 2));
            if (number < circle.getRadius()) {
                System.out.println("| Точка " + point + " находится внутри круга!");
            }
        }
    }
}
