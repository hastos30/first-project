package com.hastos.Lesson1.Task2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PointList {
    List<Point> pointList = new ArrayList<>();

    public void addNewPoint() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("| введите координаты точки");
        System.out.print("| x: ");
        double x = scanner.nextDouble();
        System.out.print("| y: ");
        double y = scanner.nextDouble();

        pointList.add(new Point(x, y));

        System.out.println("| Желаете добавить еще (1-да 2- нет)");
        int number = scanner.nextInt();
        if (number == 1) {
            System.out.println("| ваш выбор: 1");
            addNewPoint();
        } else if (number == 2) {
            System.out.println("| ваш выбор: 2");
        } else {
            System.out.println("Ошибка ввода!");
        }
    }
}
