package com.hastos.Lesson1.Task2;

import lombok.Getter;
import lombok.Setter;
import java.util.Scanner;

@Setter
@Getter
public class Circle {

    private double radius;
    private Point centre;

    Circle() {
        System.out.println("| Задайте центр окружности:");
        Scanner scanner = new Scanner(System.in);
        System.out.println("| введите координаты точки");
        System.out.print("| x: ");
        double x = scanner.nextDouble();
        System.out.print("| y: ");
        double y = scanner.nextDouble();
        centre = new Point(x, y);

        boolean error;
        do {
            error = false;
            Scanner scan = new Scanner(System.in);
            System.out.print("| Задайте радиус: ");
            if (scan.hasNextDouble()) {
                radius = scan.nextDouble();
                if (radius <= 0) {
                    System.out.println("| Радиус окружности должен быть положительным!");
                    error = true;
                }
            } else {
                System.out.println("| Ошибка ввода! Вы ввели не число, попробуйте снова.");
                error = true;
            }
        } while (error);
    }
}



